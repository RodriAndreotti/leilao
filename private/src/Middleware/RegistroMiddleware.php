<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Middleware;

/**
 * Middleware para categoria
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class RegistroMiddleware
{
    private $id;
    
    /**
     *
     * @var \Core\Repository\RegistroRepository 
     */
    private $repository;
    
    /**
     *
     * @var \Core\Repository\CategoriaRepository 
     */
    private $catRepository;
    
    public function __construct(\Core\Repository\RegistroRepository $repository, \Core\Repository\CategoriaRepository $catRepository)
    {
        $this->repository = $repository;
        $this->catRepository = $catRepository;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    
    public function post()
    {
        $registro = new \Core\Model\Registro();
        
        $this->repository->inserir($registro);
        
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        if($registro->getId()) {
            http_response_code(200);
            echo json_encode(array('registro' => $registro));
        } else {
            http_response_code(500);
            echo json_encode(array('msg'=>'Erro ao salvar registro'));
        }
    }
    
    public function get()
    {
        if($this->id) {
            $registro = $this->repository->obterPorId($this->id);
            
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/json;charset=utf-8');
            if($registro) {
                http_response_code(200);
                echo json_encode(array('registro' => $registro));
            } else {
                http_response_code(204);
                echo json_encode('');
            }
        } else {
            $registros = $this->repository->listar();
            
            foreach($registros as &$registro) {
                $registro->setCategoria($this->catRepository->obterPorId($registro->getCategoria()));
            }
            
            
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/json;charset=utf-8');
            http_response_code(200);
            echo json_encode($registros);
        }
    }
    
    public function put()
    {
        parse_str(file_get_contents("php://input"), $_PUT);
        
        $registro = new \Core\Model\Registro();
        $registro->setId(filter_var($_PUT['id'], FILTER_SANITIZE_NUMBER_INT));
        $registro->setObservacao(filter_var($_PUT['observacao']));
        
        $categoria = $this->catRepository->obterPorId(filter_var($_PUT['categoria'], FILTER_SANITIZE_NUMBER_INT));
        $registro->setCategoria($categoria);
        
        
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        if($this->repository->atualizar($registro)) {
            http_response_code(200);
            echo json_encode(array('registro' => $registro));
        } else {
            http_response_code(500);
            echo json_encode(array('msg'=>'Erro ao salvar registro'));
        }
    }
    
    public function delete()
    {
        $id = filter_var($this->id, FILTER_SANITIZE_NUMBER_INT);
        
        $registro = $this->repository->obterPorId($id);
        
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        if(!$registro) {
            http_response_code(404);
            echo json_encode(array('msg'=>'Registro não encontrado'));
        } else {
            if($this->repository->apagar($id)) {
                http_response_code(200);
                echo json_encode(array('msg'=>'Registro Apagado com sucesso'));
            } else {
                http_response_code(500);
                echo json_encode(array('msg'=>'Erro ao apagar registro'));
            }
        }
    }

}
