<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Middleware;

/**
 * Middleware para categoria
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class CategoriaMiddleware
{
    private $id;
    
    private $repository;
    
    public function __construct(\Core\Repository\CategoriaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    
    public function get()
    {
        if($this->id) {
            $categoria = $this->repository->obterPorId($this->id);
            
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/json;charset=utf-8');
            if($categoria) {
                http_response_code(200);
                echo json_encode(array('categoria' => $categoria));
            } else {
                http_response_code(204);
                echo json_encode('');
            }
        } else {
            $categorias = $this->repository->listar();
            
            
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/json;charset=utf-8');
            http_response_code(200);
            echo json_encode($categorias);
        }
    }
    
    public function post()
    {
        $categoria = new \Core\Model\Categoria();
        $categoria->setOrdem(filter_input(INPUT_POST, 'ordem', FILTER_SANITIZE_NUMBER_INT));
        $categoria->setCategoria(filter_input(INPUT_POST, 'categoria', FILTER_SANITIZE_STRING));
        $categoria->setComentario(filter_input(INPUT_POST, 'comentario', FILTER_SANITIZE_STRING));
        $categoria->setCor(filter_input(INPUT_POST, 'cor', FILTER_SANITIZE_STRING));
        
        $this->repository->inserir($categoria);
        
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        if($categoria->getId()) {
            http_response_code(200);
            echo json_encode(array('categoria' => $categoria));
        } else {
            http_response_code(500);
            echo json_encode(array('msg'=>'Erro ao salvar categoria'));
        }                
    }
    
    public function put()
    {
        parse_str(file_get_contents("php://input"), $_PUT);

        $categoria = new \Core\Model\Categoria();
        $categoria->setId(filter_var($_PUT['id'], FILTER_SANITIZE_NUMBER_INT));
        $categoria->setOrdem(filter_var($_PUT['ordem'], FILTER_SANITIZE_NUMBER_INT));
        $categoria->setCategoria(filter_var($_PUT['categoria'], FILTER_SANITIZE_STRING));
        $categoria->setComentario(filter_var($_PUT['comentario'], FILTER_SANITIZE_STRING));
        $categoria->setCor(filter_var($_PUT['cor'], FILTER_SANITIZE_STRING));
        

        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        if($this->repository->atualizar($categoria)) {
            http_response_code(200);
            echo json_encode(array('categoria' => $categoria));
        } else {
            http_response_code(500);
            echo json_encode(array('msg'=>'Erro ao salvar categoria'));
        }
    }
    
    public function delete()
    {
        
    }
}
