<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\DB;

/**
 * Classe que realiza a conexão ao banco de dados
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Connection
{

    /**
     * Instância que será armazenada para o padrão singleton
     * @var Connection 
     */
    private static $instance;
    
    /**
     * Handler do PDO PHP
     * @var \PDO 
     */
    private $pdo;

    /**
     * Inicializa a classe de conexão abrindo um handler PDO
     * Por default todos os repositórios trabalharão com transactions
     * 
     * @param \Core\Loader $app
     */
    private function __construct(DbConfig $dbConfig)
    {
        $conString = $dbConfig->getDsn();
        $this->pdo = new \PDO($conString, $dbConfig->getCredential(DbConfig::CREDENTIAL_USER), $dbConfig->getCredential(DbConfig::CREDENTIAL_SENHA));
        
        $this->pdo->setAttribute(\PDO::ATTR_ORACLE_NULLS, \PDO::NULL_TO_STRING);
        
        $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, TRUE);

        $this->pdo->beginTransaction();
    }

    /**
     * Devolte a instância da classe
     * @param \Core\Loader $app
     * @return Connection
     */
    public static function getInstance($dbConfig)
    {
        if(!self::$instance) {
            self::$instance = new self($dbConfig);
        }
        return self::$instance;
    }

    /**
     * Retorna o manipulador de banco de dados
     * @return \PDO
     */
    public function getHandler()
    {
        return $this->pdo;
    }

    /**
     * Realiza o commit se estiver em uma tansação
     */
    public function doCommit()
    {
        if ($this->getHandler()->inTransaction()) {
            $this->getHandler()->commit();
        }
    }

    /**
     * Realiza o rollback se estiver em uma tansação
     */
    public function doRollback()
    {
        if ($this->getHandler()->inTransaction()) {
            $this->getHandler()->rollBack();
        }
    }

}
