<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\DB;

/**
 * Description of DbConfig
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class DbConfig
{
    private $user;
    private $driver;
    private $senha;
    private $dbName;
    private $charset;
    private $host;
    
    const CREDENTIAL_USER = 1, CREDENTIAL_SENHA = 2;
    
    public function __construct($user, $senha, $dbName, $host, $driver = 'mysql', $charset = 'utf8')
    {
        $this->user = $user;
        $this->driver = $driver;
        $this->senha = $senha;
        $this->dbName = $dbName;
        $this->charset = $charset;
        $this->host = $host;
    }

    
    public function getDsn()
    {
        if(!$this->host || !$this->driver) {
            throw \Exception('Driver ou host não informado');
        }
        
        $conString = $this->driver . ':host=' . $this->host . ';dbname=' . $this->dbName . ';charset=' . $this->charset;
        
        return $conString;
    }
    
    public function getCredential($cred)
    {
        if(!$this->user & !$this->senha) {
            throw new \Exception('Credenciais do banco de dados não foram configuradas');
        }
        
        if($cred == DbConfig::CREDENTIAL_USER) {
            if($this->user) {
                return $this->user;
            }
            
        } elseif ($cred == DbConfig::CREDENTIAL_SENHA) {
            if(!is_null($this->senha)) {
                return $this->senha;
            }
        }
        
        throw new \Exception('Credencial inexistente');
    }
}
