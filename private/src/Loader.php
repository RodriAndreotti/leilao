<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Loader com informações diversas do sistema
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Loader
{
    /**
     * Array associativo com configurações do sistema
     * @var array 
     */
    private $config;
    
    
    public function __construct()
    {
        $this->loadConfig();
    }

    /**
     * Obtém uma configuração pela chave
     * @param string $key
     */
    public function getConfig($key)
    {
        if (is_array($this->config)) {
            if (array_key_exists($key, $this->config)) {
                return $this->config[$key];
            } else {
                throw new \Exception('Configuração não encontrada', 1404);
            }
        } else {
            throw new \Exception('Arquivo de configuração inválido.');
        }
    }

    /**
     * Retorna o caminho absoluto até a raíz do sistema
     * @return string
     */
    public function getAbsolutePath()
    {
        return realpath(__DIR__ . str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, '/../'));
    }

    /**
     * Carregas as configurações do sistema
     * @throws \Exception
     */
    private function loadConfig()
    {
        if (!file_exists($this->getAbsolutePath() . '/config/system.config.php')) {
            throw new \Exception('Arquivo de configuração do sistema não encontrado!'
                    . '\n<br>Verifique a existência do arquivo system.config.php no diretório config.');
        }
        $this->config = require_once $this->getAbsolutePath() . '/config/system.config.php';
    }
}
