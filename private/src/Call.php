<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core;

/**
 * Call geral do sistema
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Call
{

    public function dispatch()
    {
        $route = trim(str_replace('middleware', '', filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_STRING)), '/');

        $routeParts = explode('/', $route);

        $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');


        if (isset($routeParts[0])) {
            $middleware = strtolower($routeParts[0]);
        }

        if (isset($routeParts[1])) {
            if (is_numeric($routeParts[1])) {
                $id = strtolower($routeParts[1]);
            } else {
                //$action = strtolower($routeParts[1]);
            }
        }

        if (isset($routeParts[2])) {
            $id = strtolower($routeParts[2]);
        } elseif (!$id) {
            $id = 0;
        }


        if (!isset($action)) {
            $action = strtolower($method);
        }

        $loader = new Loader();

        $dbConfig = new DB\DbConfig($loader->getConfig('db_user'), $loader->getConfig('db_passwd'), $loader->getConfig('db_name'), $loader->getConfig('db_host'));

        $catRep = Repository\CategoriaRepository::getInstance(DB\Connection::getInstance($dbConfig));
        switch ($middleware) {
            case 'categoria':
                $obj = new Middleware\CategoriaMiddleware($catRep);
                break;
            case 'registro':
                $repository = Repository\RegistroRepository::getInstance(DB\Connection::getInstance($dbConfig));
                $obj = new Middleware\RegistroMiddleware($repository, $catRep);
                break;
            default: throw new \Exception('Middleware não suportado');
        }

        if ($obj) {
            if ($id) {
                $obj->setId($id);
            }
            return $obj->$action();
        }
    }

}
