<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Traits;

/**
 * Trait para serializar as entidades
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
trait JsonSerializeTrait
{

    /**
     * Serializa os dados
     * @ignore
     * @return type
     */
    public function jsonSerialize()
    {
        $data = array();
        
        foreach ($this as $key => $value) {
            if (is_object($value)) {
                if ($value instanceof \Doctrine\Common\Collections\Collection) {
                    $data[$key] = $value->toArray();
                } elseif ($value instanceof \JsonSerializable) {
                    $data[$key] = $value->jsonSerialize();
                } elseif ($value instanceof \DateTime) {
                    $data[$key] = $value->format(\DateTimeInterface::ISO8601);
                }
            } else {
                $data[$key] = $value;
            }
        }

        return $data;
    }

}
