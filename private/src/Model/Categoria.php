<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Model;

/**
 * Description of Categoria
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Categoria implements \JsonSerializable
{
    
    use \Core\Traits\JsonSerializeTrait;
    
    private $id;
    
    private $ordem;
    
    private $categoria;
    
    private $cor;
    
    private $comentario;
    
    public function getId()
    {
        return $this->id;
    }

    

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getCor()
    {
        return $this->cor;
    }

    public function getComentario()
    {
        return $this->comentario;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function setCor($cor)
    {
        $this->cor = $cor;
    }

    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }
    
    public function getOrdem()
    {
        return $this->ordem;
    }

    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }


}
