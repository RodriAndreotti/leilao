<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Model;

/**
 * Etidade do registro
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class Registro implements \JsonSerializable
{

    use \Core\Traits\JsonSerializeTrait;

    private $id;
    private $observacao;
    private $categoria;

    public function getId()
    {
        return $this->id;
    }

    public function getObservacao()
    {
        return $this->observacao;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setObservacao($conteudo)
    {
        $this->observacao = $conteudo;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

}
