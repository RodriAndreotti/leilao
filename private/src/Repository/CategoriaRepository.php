<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Repository;

/**
 * Repositório da Categoria
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class CategoriaRepository
{
    /**
     *
     * @var \Core\DB\Connection 
     */
    private $conn;
    
    /**
     * 
     * @var CategoriaRepository
     */
    public static $instance;
    
    private function __construct(\Core\DB\Connection $conn)
    {
        $this->conn = $conn;
    }
    
    /**
     * Obtém a instância do repositório
     * @param \Core\DB\Connection $conn
     * @return CategoriaRepository
     */
    public static function getInstance(\Core\DB\Connection $conn)
    {
        if(!self::$instance) {
            self::$instance = new self($conn);
        }
        
        return self::$instance;
    }

    /**
     * Lista categorias
     * @return array
     */
    public function listar()
    {
        $sql = 'SELECT * FROM categoria ORDER BY ordem ASC';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt->execute()) {
            $categorias = $stmt->fetchAll(\PDO::FETCH_CLASS, \Core\Model\Categoria::class);
            return $categorias;
        }
        
        return null;
    }
    
    /**
     * Obtém categoria por ID
     * @param type $id
     * @return \Core\Model\Categoria
     */
    public function obterPorId($id)
    {
        $sql = 'SELECT * FROM categoria WHERE id = :id';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        $stmt->bindValue('id', $id, \PDO::PARAM_INT);
        
        if($stmt->execute()) {
            $categoria = $stmt->fetchObject(\Core\Model\Categoria::class);
            
            return $categoria;
        
        }
        return null;
    }
    
    /**
     * Apaga categoria por ID
     * @param integer $id
     * @return boolean
     */
    public function apagar($id)
    {
        $sql = 'DELETE FROM categoria WHERE id = :id';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        $stmt->bindValue('id', $id, \PDO::PARAM_INT);
        
        return $stmt->execute();
    }
    
    /**
     * Insere uma nova categoria
     * @param Categoria $categoria
     * @return \Core\Model\Categoria
     */
    public function inserir(\Core\Model\Categoria $categoria)
    {
        $sql = 'INSERT INTO categoria'
                . '(ordem, categoria, cor, comentario)'
                . ' VALUES '
                . '(:ordem, :categoria, :cor, :comentario)';
        
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt) {
            $stmt->bindValue('ordem', $categoria->getOrdem(), \PDO::PARAM_INT);
            $stmt->bindValue('categoria', $categoria->getCategoria(), \PDO::PARAM_STR);
            $stmt->bindValue('cor', $categoria->getCor(), \PDO::PARAM_STR);
            $stmt->bindValue('comentario', $categoria->getComentario(), \PDO::PARAM_STR);
            
            if($stmt->execute()) {
                $categoria->setId($this->conn->getHandler()->lastInsertId());
                $this->conn->doCommit();
            } else {
                
                $this->conn->doRollback();
            }
        }
        
       
        return $categoria;
    }
    
    /**
     * Atualiza uma categoria
     * @param \Core\Model\Categoria $categoria
     * @return \Core\Model\Categoria
     */
    public function atualizar(\Core\Model\Categoria $categoria)
    {
        $sql = 'UPDATE categoria SET '
                . 'ordem = :ordem, categoria = :categoria, cor = :cor, comentario = :comentario '
                . 'WHERE id = :id';
        
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt) {
            $stmt->bindValue('ordem', $categoria->getOrdem(), \PDO::PARAM_INT);
            $stmt->bindValue('categoria', $categoria->getCategoria(), \PDO::PARAM_STR);
            $stmt->bindValue('cor', $categoria->getCor(), \PDO::PARAM_STR);
            $stmt->bindValue('comentario', $categoria->getComentario(), \PDO::PARAM_STR);
            $stmt->bindValue('id', $categoria->getId(), \PDO::PARAM_INT);

            
            if($stmt->execute()) {
                $this->conn->doCommit();
                return true;
            } else {
                $this->conn->doRollback();
            }
        }
        
        return false;
    }
}
