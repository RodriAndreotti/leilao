<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Repository;

/**
 * Repositório do Registro
 *
 * @author Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 */
class RegistroRepository
{
    
    /**
     *
     * @var \Core\DB\Connection 
     */
    private $conn;
    
    /**
     * 
     * @var RegistroRepository
     */
    public static $instance;
    
    private function __construct(\Core\DB\Connection $conn)
    {
        $this->conn = $conn;
    }
    
    /**
     * Obtém a instância do repositório
     * @param \Core\DB\Connection $conn
     * @return RegistroRepository
     */
    public static function getInstance(\Core\DB\Connection $conn)
    {
        if(!self::$instance) {
            self::$instance = new self($conn);
        }
        
        return self::$instance;
    }
    
    /**
     * Lista todos os registros
     * @return array
     */
    public function listar()
    {
        $sql = 'SELECT * FROM registro';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt->execute()) {
            $registros = $stmt->fetchAll(\PDO::FETCH_CLASS, \Core\Model\Registro::class);
            return $registros;
        }
        
        return null;
    }
    
    /**
     * Lista todos por status
     * @param integer $status
     * @return array
     */
    public function listarPorCategoria($registro)
    {
        $sql = 'SELECT * FROM registro WHERE categoria = :categoria';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        $stmt->bindValue('categoria', $registro, \PDO::PARAM_INT);
        
        if($stmt->execute()) {
            $registros = $stmt->fetchAll(\PDO::FETCH_CLASS, \Core\Model\Registro::class);
            return $registros;
        }
        
        return null;
    }
    
    /**
     * Obtém registro por ID
     * @param integer $id
     * @return \Core\Model\Registro
     */
    public function obterPorId($id)
    {
        $sql = 'SELECT * FROM registro WHERE id = :id';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        $stmt->bindValue('id', $id, \PDO::PARAM_INT);
        
        if($stmt->execute()) {
            $registro = $stmt->fetchObject(\Core\Model\Registro::class);
            
            return $registro;
        
        }
        return null;
    }
    
    /**
     * Apaga registro por ID
     * @param integer $id
     * @return boolean
     */
    public function apagar($id)
    {
        $sql = 'DELETE FROM registro WHERE id = :id';
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        $stmt->bindValue('id', $id, \PDO::PARAM_INT);
        $result = (boolean)$stmt->execute();
        $this->conn->doCommit();
        
        return $result;
    }
    
    /**
     * Insere um novo registro
     * @param \Core\Model\Registro $registro
     * @return \Core\Model\Registro
     */
    public function inserir(\Core\Model\Registro $registro)
    {
        $sql = 'INSERT INTO registro'
                . '(categoria, observacao)'
                . ' VALUES '
                . '(:categoria, :observacao)';
        
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt) {
            $stmt->bindValue('categoria', ($registro->getCategoria() ? $registro->getCategoria()->getId() : null), \PDO::PARAM_INT);
            $stmt->bindValue('observacao', $registro->getObservacao(), \PDO::PARAM_STR);
            
            if($stmt->execute()) {
                $registro->setId($this->conn->getHandler()->lastInsertId());
                $this->conn->doCommit();
            } else {
                $this->conn->doRollback();
            }
        }
        
        return $registro;
    }
    
    /**
     * Atualiza um registro
     * @param \Core\Model\Registro $registro
     * @return boolean
     */
    public function atualizar(\Core\Model\Registro $registro)
    {
        $sql = 'UPDATE registro SET '
                . 'categoria = :categoria, observacao = :observacao '
                . 'WHERE id = :id';
        
        
        $stmt = $this
                ->conn
                ->getHandler()
                ->prepare($sql);
        
        if($stmt) {
            $stmt->bindValue('categoria', ($registro->getCategoria() ? $registro->getCategoria()->getId() : null), \PDO::PARAM_INT);
            $stmt->bindValue('observacao', $registro->getObservacao(), \PDO::PARAM_STR);
            $stmt->bindValue('id', $registro->getId(), \PDO::PARAM_INT);

            
            if($stmt->execute()) {
                $this->conn->doCommit();
                return true;
            } else {
                var_dump($stmt->errorInfo());
                
                $this->conn->doRollback();
            }
        }
        
        return false;
    }
}
