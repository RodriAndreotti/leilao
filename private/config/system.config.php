<?php

$systemConfig = array();


$systemConfig['publicDir'] = '/public';
$systemConfig['privateDir'] = '/private';

// Banco de dados
$systemConfig['db_host'] = 'localhost';
$systemConfig['db_user'] = 'root';
$systemConfig['db_passwd'] = '';
$systemConfig['db_name'] = 'leilao';



return $systemConfig;
