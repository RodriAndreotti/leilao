/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Rodrigo Teixeira Andreotti <ro.andriotti@gmail.com>
 * Created: 10 de jun de 2019
 */

CREATE TABLE `categoria` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ordem` INT NOT NULL,
  `categoria` VARCHAR(75) NULL,
  `cor` VARCHAR(9) NULL,
  `comentario` VARCHAR(500) NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `registro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categoria` INT NULL,
  `observacao` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_categoria_idx` (`categoria` ASC) VISIBLE,
  CONSTRAINT `fk_categoria`
    FOREIGN KEY (`categoria`)
    REFERENCES `categoria` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
COMMENT = '';
