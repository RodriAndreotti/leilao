<?php
// www/routing.php
if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    return false;
} elseif (preg_match('/middleware/', $_SERVER["REQUEST_URI"])) {
    include __DIR__ . '/call.php';
} else {
    return false;
}