/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var categorias = [];
var registros = [];

var dTable;
// Funções

function buscaCategoria(idCategoria)
{
    let size = categorias.length;
    for(i = 0; i < size; i++) {
        if(categorias[i].id == parseInt(idCategoria)) return categorias[i];
    }
    return null;
}

/**
 * Carrega / recarrega categorias
 * @return {void}
 */
function carregarCategorias(edita)
{
    $.ajax({
        url: '/middleware/categoria',
        type: 'get',
        async: true,
        success: function (response) {
            if (response) {
                if (Array.isArray(response)) {
                    categorias = response;
                    popularCategorias(edita);
                }
            }
        }
    });
}

function carregarRegistros()
{
    $.ajax({
        url: '/middleware/registro',
        type: 'get',
        async: true,
        success: function (response) {
            if (response) {
                if (Array.isArray(response)) {
                    registros = response;
                    pupularRegistros();
                }
            }
        }
    });
}

function pupularRegistros()
{
    let size = registros.length;
    for (i = 0; i < size; i++) {
        let linha = $('<tr>');
        
        let colCor = $('<td>');
        colCor.addClass('cor');
        if(registros[i].categoria) {
            if(registros[i].categoria.cor) {
                colCor.css('background-color', registros[i].categoria.cor);
            }
        }
        $(linha).append(colCor);
        
        let colId = $('<td>');


        let text = registros[i].observacao.replace(/(<([^>]+)>)/ig, '');
        var currentLength = text.length;
        var maxlength = 512;

        let leftLength = maxlength - currentLength;


        colId.addClass('id');
        colId.attr('id',registros[i].id);
        colId.attr('data-idCat',registros[i].categoria ? registros[i].categoria.id : '0');
        $(colId).text(registros[i].categoria ? registros[i].categoria.ordem : '');
        $(linha).append(colId);

        let colTexto = $('<td>');
        $(colTexto).append('<div contenteditable="true" class="text countit" data-maxlength="512" style="width: 100%; min-height: 85px;">' + registros[i].observacao + '</div>\n\
                                        <span class="input-footer">\n\
                                            <strong>caracteres restantes:</strong> <span class="rem_post" title="' + leftLength + '">' + leftLength + '</span> de 512\n\
                                            <button type="button" style="float: right;" class="btn btn-xs btn-default btnRemoveRegistro"><i class="fa fa-trash-o"></i></button>\n\
                                        </span>')

        $(linha).append(colTexto);

        let colOpcoes = $('<td>');
        $(colOpcoes).append('<div class="row mt5 text-center">\n\
                                                            <div class="col-xs-3">\n\
                                                                <div class="btn-group">\n\
                                                                    <button class="btn btn-sm btn-info btn-block light btnCopy">COPIAR</button>\n\
                                                                </div>\n\
                                                            </div>\n\
                                                        </div>\n\
                                                        <div class="row mt5 text-center">\n\
                                                            <div class="col-xs-3">\n\
                                                                <div class="form-group form-group-sm">\n\
                                                                    <select \n\
                                                                        class="select-categoria" \n\
                                                                        style="display: none;" \n\
                                                                        data-selected="' + (registros[i].categoria ? registros[i].categoria.id : '') + '">\n\
                                                                        </select>\n\
                                                                </div>\n\
                                                            </div>\n\
                                                        </div>');
        $(linha).append(colOpcoes);

        CKEDITOR.inline($(colTexto).find('.text').get(0));
        dTable.row.add($(linha)).draw();

    }
}

/**
 * Popula campos com categorias
 * @return {void}
 */
function popularCategorias(edita)
{
    if(typeof(edita) == 'undefined') {
        edita = false;
    }
    $('#txtId').empty();

    let size = categorias.length;
    let html = '<option value="">Selecione...</option>';
    for (i = 0; i < size; i++) {
        html += '<option value="' + categorias[i].id + '">' + categorias[i].categoria.toUpperCase() + '</option>';
    }
    
    $('#txtId').html(html);

    $.each($('.select-categoria'), function (i, item) {
        let linha = $(item).parent().parent().parent().parent().parent();
        let idCat = $(linha).find('td.id').attr('data-idCat');
        
        if($(item).val() != '') {
            if(edita) {
                let categoria = buscaCategoria(idCat);

                linha.find('td.id').text(categoria.ordem);
                if(categoria.cor) {
                    linha.find('.cor').css('background-color', categoria.cor);
                }
            }
        }
        $(item).html(html);
        $(item).val($(item).data('selected'));
    });

    $('.select-categoria').multiselect({
        buttonClass: 'multiselect dropdown-toggle btn btn-sm btn-primary btn-block'
    });
}


// Listeners
$(document).ready(function () {
    $.LoadingOverlay("show", {
        image: "",
        fontawesome: "fa fa-spinner fa-pulse "
    });

    

    "use strict";

    // Init Theme Core
    Core.init();

    // Init Theme Core
    Demo.init();

    // Init tray navigation smooth scroll
    $('.tray-nav a').smoothScroll({
        offset: -145
    });

    // Custom tray navigation animation
    setTimeout(function () {
        $('.custom-nav-animation li').each(function (i, e) {
            var This = $(this);
            var timer = setTimeout(function () {
                This.addClass('animated zoomIn');
            }, 100 * i);
        });
    }, 600);


    // Init Datatables with Tabletools Addon
    dTable = $('#datatable3').DataTable({
        "aoColumnDefs": [{
                'bSortable': true,
                'aTargets': [-1]
            }],
        "paging": false,
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Próximo"
            }
        },
        "iDisplayLength": 5,
        "aLengthMenu": [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
    });

    // Add Placeholder text to datatables filter bar
    $('.dataTables_filter input').attr("placeholder", "Buscar...");

    carregarRegistros();
    carregarCategorias();


    var modalContent = $('#modal-content');

    modalContent.on('click', '.holder-style', function (e) {
        e.preventDefault();

        modalContent.find('.holder-style').removeClass('holder-active');
        $(this).addClass('holder-active');
    });

    $('.demo-auto').colorpicker();



    $.LoadingOverlay("hide");

});


CKEDITOR.on('instanceCreated', function (event) {
    var editor = event.editor;

    editor.on('configLoaded', function () {
        editor.config.toolbarGroups = [
            {name: 'document', groups: ['mode', 'document', 'doctools']},
            {name: 'clipboard', groups: ['clipboard', 'undo']},
            {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
            {name: 'forms', groups: ['forms']},
            {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
            {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
            {name: 'links', groups: ['links']},
            {name: 'insert', groups: ['insert']},
            {name: 'styles', groups: ['styles']},
            {name: 'colors', groups: ['colors']},
            {name: 'tools', groups: ['tools']},
            {name: 'others', groups: ['others']},
            {name: 'about', groups: ['about']}
        ];

        editor.config.removeButtons = 'Source,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CopyFormatting,NumberedList,Outdent,Indent,BulletedList,Blockquote,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,BidiLtr,Link,Unlink,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Format,Font,FontSize,Maximize,ShowBlocks,About,Language,BidiRtl,Checkbox';
    });

    /**
     * Salva texto
     */
    let lastTimeout;
    editor.on('change', function () {
        let text = editor.getData().replace(/(<([^>]+)>)/ig, '');

        var maxlength = parseInt($(editor.element.$).attr("data-maxlength"));
        var currentLength = text.length;

        if (currentLength >= maxlength) {
            editor.setData(editor.getData().substr(0, maxlength));
        }

        $(editor.element.$).parent().find('.rem_post').text(maxlength - currentLength);

        if (lastTimeout) {
            clearTimeout(lastTimeout);
        }
        lastTimeout = setTimeout(function () {
            let linha = $(editor.element.$).parent().parent(); // procura a linha da tabela
            let categoria = $(linha).find('.select-categoria').val();
            let id = parseInt(linha.find('td.id').attr('id'));
            let observacao = editor.getData();

            let dados = {
                id: id,
                categoria: categoria,
                observacao: observacao
            }

            $.ajax({
                url: '/middleware/registro',
                type: 'put',
                dataType: 'json',
                data: dados,
                success: function (response) {
                    if (!response) {
                        alert('Erro ao salvar registro');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

        }, 10000) // Aguarda 10 segundos de inatividade
    });
    
    editor.on('blur', function () {
        if (lastTimeout) {
            clearTimeout(lastTimeout);
        }
        let linha = $(editor.element.$).parent().parent(); // procura a linha da tabela
        let categoria = $(linha).find('.select-categoria').val();
        let id = parseInt(linha.find('td.id').attr('id'));
        let observacao = editor.getData();

        let dados = {
            id: id,
            categoria: categoria,
            observacao: observacao
        }

        $.ajax({
            url: '/middleware/registro',
            type: 'put',
            dataType: 'json',
            data: dados,
            success: function (response) {
                if (!response) {
                    alert('Erro ao salvar registro');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    });
});

/**
 * Adiciona novo registro
 */
$('#btnNovoRegistro').on('click', function (e) {
    e.preventDefault();

    $.ajax({
        url: '/middleware/registro',
        type: 'post',
        dataType: 'json',
        beforeSend: function () {
            $.LoadingOverlay("show", {
                image: "",
                fontawesome: "fa fa-spinner fa-pulse "
            });
        },
        success: function (response) {
            if (response) {
                if (response.registro) {
                    let registro = response.registro;

                    let linha = $('<tr>');
                    let colCor = $('<td>');
                    colCor.addClass('cor');
                    $(linha).append(colCor);
                    
                    let colId = $('<td>');
                    colId.addClass('id');
                    colId.attr('id',response.registro.id);
                    colId.attr('data-idCat',response.registro.categoria ? registros[i].categoria.id : '0');
                    $(colId).text(response.registro.categoria ? response.registro.categoria.ordem : '');
                    $(linha).append(colId);

                    let colTexto = $('<td>');
                    $(colTexto).append('<div contenteditable="true" class="text countit" data-maxlength="512" style="width: 100%; min-height: 85px;"></div>\n\
                                        <span class="input-footer">\n\
                                            <strong>caracteres restantes:</strong> <span class="rem_post" title="512">512 </span> de 512\n\
                                            <button type="button" style="float: right;" class="btn btn-xs btn-default btnRemoveRegistro"><i class="fa fa-trash-o"></i></button>\n\
                                        </span>')

                    $(linha).append(colTexto);

                    let colOpcoes = $('<td>');
                    $(colOpcoes).append('<div class="row mt5 text-center">\n\
                                                            <div class="col-xs-3">\n\
                                                                <div class="btn-group">\n\
                                                                    <button class="btn btn-sm btn-info btn-block light btnCopy">COPIAR</button>\n\
                                                                </div>\n\
                                                            </div>\n\
                                                        </div>\n\
                                                        <div class="row mt5 text-center">\n\
                                                            <div class="col-xs-3">\n\
                                                                <div class="form-group form-group-sm">\n\
                                                                    <select class="select-categoria" style="display: none;"></select>\n\
                                                                </div>\n\
                                                            </div>\n\
                                                        </div>');
                    $(linha).append(colOpcoes);

                    let size = categorias.length;
                    let html = '<option value="">Selecione...</option>';
                    for (i = 0; i < size; i++) {
                        html += '<option value="' + categorias[i].id + '">' + categorias[i].categoria.toUpperCase() + '</option>';
                    }

                    
                    $(linha).find('.select-categoria').html(html);
                    

                    $('#datatable3 > tbody').append(linha);

                    CKEDITOR.inline($(colTexto).find('.text').get(0));
                    
                    dTable.row.add($(linha)).draw();

                    $('.select-categoria').multiselect({
                        buttonClass: 'multiselect dropdown-toggle btn btn-sm btn-primary btn-block'
                    });
                } else {
                    alert('Erro ao inserir registro');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    })
});


/**
 * Seleciona categoria para edição
 */
$(document).off('change keyup', '#txtId').on('change keyup', '#txtId', function (e) {
    let id = $(this).val();

    if (id) {
        $.ajax({
            url: '/middleware/categoria/' + id,
            type: 'get',
            beforeSend: function () {
                $.LoadingOverlay("show", {
                    image: "",
                    fontawesome: "fa fa-spinner fa-pulse "
                });
            },
            success: function (response) {
                if (response) {
                    let categoria = response.categoria;
                    if (categoria) {
                        $('#txtId').val(categoria.id);
                        $('#categoria').val(categoria.categoria);
                        $('#comentario').val(categoria.comentario);
                        $('#ordem').val(categoria.ordem);
                        $('#cor').val(categoria.cor);
                        
                        $("#cor").val(categoria.cor);
                        $('.demo-auto').colorpicker({color : categoria.cor})
                        $('.demo-auto').colorpicker('setValue', categoria.cor)
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    }
});


/**
 * Abre Modal para cadastro de categoria
 */
$('#btnCriaCategoria').on('click', function (e) {
    e.preventDefault();

    $('#txtId').parent().hide();
    $('#txtAcavo').text('Cadastrar');
    $('#modalCategoria').modal('show');
});


/**
 * Abre modal para edição de categoria
 */
$('#btnEditaCategoria').on('click', function (e) {
    e.preventDefault();

    $('#txtId').parent().show();
    $('#txtAcao').text('Editar');
    $('#modalCategoria').modal('show');
});


/**
 * Salva uma categoria
 */
$(document).off('click', '#btnSalvarCategoria').on('click', '#btnSalvarCategoria', function (e) {
    e.preventDefault();
    let categoria = {};
    categoria.id = $('#txtId').val();
    categoria.categoria = $('#categoria').val();
    categoria.comentario = $('#comentario').val();
    categoria.ordem = $('#ordem').val();
    categoria.cor = $('#cor').val();

    let edita = categoria.id ? true : false;

    $.ajax({
        url: '/middleware/categoria',
        type: (edita ? 'put' : 'post'),
        dataType: 'json',
        data: categoria,
        beforeSend: function () {
            $.LoadingOverlay("show", {
                image: "",
                fontawesome: "fa fa-spinner fa-pulse "
            });
        },
        success: function (response) {
            if (response) {
                if (response.categoria.id) {
                    $('#txtId').val('');
                    $('#categoria').val('');
                    $('#comentario').val('');
                    $('#ordem').val('');
                    $('#cor').val('');
                    carregarCategorias(edita);
                    $('#modalCategoria').modal('hide');

                    alert('Categoria salva com sucesso');

                } else {
                    alert('Erro ao salvar categoria');
                }
            }
            $.LoadingOverlay("hide");
        },
        error: function () {
            alert('Erro ao salvar categoria');
            $.LoadingOverlay("hide");
        }
    });

});


$(document).off('keyup change', '.select-categoria').on('keyup change', '.select-categoria', function (e) {
    let linha = $(this).parent().parent().parent().parent().parent();
    let categoria = $(this).val();
    let id = parseInt(linha.find('td.id').attr('id'));
    let observacao = linha.find('.text').html();

    let dados = {
        id: id,
        categoria: categoria,
        observacao: observacao
    }


    $.ajax({
        url: '/middleware/registro',
        type: 'put',
        dataType: 'json',
        data: dados,
        success: function (response) {
            if (!response) {
                alert('Erro ao salvar registro');
            } else {
                if(response.registro.categoria) {
                    linha.find('.id').text(response.registro.categoria.ordem);
                    if(response.registro.categoria.cor) {
                        linha.find('.cor').css('background-color', response.registro.categoria.cor);
                    } else {
                        linha.find('.cor').css('background-color', 'transparent');
                    }
                }
                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        },

    });
});

$(document).off('click', '.btnRemoveRegistro').on('click', '.btnRemoveRegistro', function (e) {
    if (confirm('Atenção:\nEsta ação não poderá ser desfeita!\ntem certeza que deseja continuar?')) {
        let linha = $(this).parent().parent().parent();
        let id = $(linha).find('td.id').attr('id');

        $.ajax({
            url: '/middleware/registro/' + id,
            type: 'delete',
            dataType: 'json',
            beforeSend: function () {
                $.LoadingOverlay("show", {
                    image: "",
                    fontawesome: "fa fa-spinner fa-pulse "
                });
            },
            success: function (response) {
                if (response) {
                    $(linha).remove();
                    alert('Registro excluído com sucesso');
                } else {
                    alert('Erro ao excluir registro');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    }
});

// Botão Copiar
$(document).off('click', '.btnCopy').on('click', '.btnCopy', function (e) {
    e.preventDefault();
    let linha = $(this).parent().parent().parent().parent().parent();
    let idLinha = $(linha).index() + 1;
    let editor = CKEDITOR.instances['editor' + idLinha];


    editor.focus();
    let conteudo = editor.getData();
    let dummyInput = $('<textarea>').val(conteudo).appendTo('body').select();
    document.execCommand("Copy");
    dummyInput.remove();
    alert("Copiado para a área de transferência!");
});
// /copy to clipboard